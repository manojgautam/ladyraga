<?php
/**
 * The template for displaying the footer
 */

	$id = is_singular() ? get_the_ID() : 0;
	$effects_params = thegem_get_sanitize_page_effects_data($id);
	if($effects_params['effects_parallax_footer']) {
		wp_enqueue_script('thegem-parallax-footer');
	}
?>

		</div><!-- #main -->

		<?php if(!$effects_params['effects_page_scroller'] && !$effects_params['effects_hide_footer']) : ?>
			<?php if($effects_params['effects_parallax_footer']) : ?><div class="parallax-footer"><?php endif; ?>
			<?php if(is_active_sidebar('footer-widget-area')) : ?>
			<footer id="colophon" class="site-footer" role="contentinfo">
				<div class="container">
					<?php get_sidebar('footer'); ?>
				</div>
			</footer><!-- #colophon -->
			<?php endif; ?>

			<?php if(thegem_get_option('footer_active')) : ?>
	<footer  class="site-footer-card">
				<div class="container"><div class="row">
						<div class="col-md-3"><h3>WE ACCEPT</h3></div>
						<div class="col-md-9"><div class="we-accept">
   <img class="alignleft wp-image-6246 size-full" src="<?php echo get_template_directory_uri(); ?>/images/cards/pay_u_biz.jpg" alt="pay_u_biz" >
   <img class="alignleft wp-image-6244 size-full" src="<?php echo get_template_directory_uri(); ?>/images/cards/pay_u_money.jpg" alt="pay_u_money">
   <img class="alignleft wp-image-6240 size-full" src="<?php echo get_template_directory_uri(); ?>/images/cards/paytm.jpg" alt="paytm" >
   <img class="alignleft wp-image-6238 size-full" src="<?php echo get_template_directory_uri(); ?>/images/cards/money-icon-5.jpg" alt="money-icon-5" >
   <img class="alignleft wp-image-6238 size-full" src="<?php echo get_template_directory_uri(); ?>/images/cards/visa.jpg" alt="visa" >
   <img class="alignleft wp-image-6238 size-full" src="<?php echo get_template_directory_uri(); ?>/images/cards/master.jpg" alt="master-card" >
   <img class="alignleft wp-image-6238 size-full" src="<?php echo get_template_directory_uri(); ?>/images/cards/COD.jpg" alt="cod" >
   <img class="alignleft wp-image-6238 size-full" src="<?php echo get_template_directory_uri(); ?>/images/cards/rupay.jpg" alt="rupay" >
   
   </div></div>
				</div></div>
				
				</footer>
				<footer  class="site-footer-card footer-brands">
				<div class="container"><div class="row"><div class="col-sm-12">
					<?php if(has_nav_menu('footer')) : ?>
						<nav id="footer-navigation" class="site-navigation footer-navigation centered-box" role="navigation">
							<?php wp_nav_menu(array('theme_location' => 'footer', 'menu_id' => 'footer-menu', 'menu_class' => 'nav-menu styled clearfix inline-inside', 'container' => false, 'depth' => 1, 'walker' => new thegem_walker_footer_nav_menu)); ?>
						</nav>
						<?php endif; ?>
				</div></div></div>
				</footer>
			<footer id="footer-nav" class="site-footer">
				<div class="container"><div class="row">

					<div class="col-md-6"><div class="footer-site-info"><?php echo wp_kses_post(do_shortcode(nl2br(stripslashes(thegem_get_option('footer_html'))))); ?></div></div>

					<div class="col-md-3">
						<?php if(has_nav_menu('footer')) : ?>
						<nav id="footer-navigation" class="site-navigation footer-navigation centered-box" role="navigation">
							<?php //wp_nav_menu(array('theme_location' => 'footer', 'menu_id' => 'footer-menu', 'menu_class' => 'nav-menu styled clearfix inline-inside', 'container' => false, 'depth' => 1, 'walker' => new thegem_walker_footer_nav_menu)); ?>
						</nav>
						<?php endif; ?>

					</div>
					<div class="col-md-3">
						<?php
							$socials_icons = array('facebook' => thegem_get_option('facebook_active'), 'linkedin' => thegem_get_option('linkedin_active'), 'twitter' => thegem_get_option('twitter_active'), 'instagram' => thegem_get_option('instagram_active'), 'pinterest' => thegem_get_option('pinterest_active'), 'googleplus' => thegem_get_option('googleplus_active'), 'stumbleupon' => thegem_get_option('stumbleupon_active'), 'rss' => thegem_get_option('rss_active'), 'vimeo' => thegem_get_option('vimeo_active'), 'youtube' => thegem_get_option('youtube_active'), 'flickr' => thegem_get_option('flickr_active'));
							if(in_array(1, $socials_icons)) : ?>
							<div id="footer-socials"><div class="socials inline-inside socials-colored">
									<?php foreach($socials_icons as $name => $active) : ?>
										<?php if($active) : ?>
											<a href="<?php echo esc_url(thegem_get_option($name . '_link')); ?>" target="_blank" title="<?php echo esc_attr($name); ?>" class="socials-item"><i class="socials-item-icon <?php echo esc_attr($name); ?>"></i></a>
										<?php endif; ?>
									<?php endforeach; ?>
									<?php do_action('thegem_footer_socials'); ?>
							</div></div><!-- #footer-socials -->
						<?php endif; ?>
					</div>

					

				</div></div>
			</footer><!-- #footer-nav -->
			<?php endif; ?>
			<?php if($effects_params['effects_parallax_footer']) : ?></div><!-- .parallax-footer --><?php endif; ?>

		<?php endif; ?>

	</div><!-- #page -->

	<?php wp_footer(); ?>
<script type='text/javascript' src='<?php echo bloginfo('template_directory')?>/js/remodal.min.js'></script>
<script type="text/javascript" src="<?php echo bloginfo('template_directory')?>/js/jquery.smartWizard.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script src="<?php echo bloginfo('template_directory')?>/js/clipboard.min.js"></script>
<script>
var clipboard = new Clipboard('.btn');

clipboard.on('success', function(e) {
    console.info('Action:', e.action);
    console.info('Text:', e.text);
    console.info('Trigger:', e.trigger);

    e.clearSelection();
});

clipboard.on('error', function(e) {
    console.error('Action:', e.action);
    console.error('Trigger:', e.trigger);
});
</script>

<script>
jQuery(document).ready(function(){
    jQuery('input[type="checkbox"]').click(function(){
		 if (jQuery(this).is(":checked")) {
                jQuery("p#giftmess_field ").show();
            } else {
                jQuery("p#giftmess_field").hide();
            }
    });
});
</script>
<script>
jQuery('.remodal-close').click(function () {
jQuery("#show_res").hide();
jQuery("#callus").show();
});
</script>

<script>
jQuery(".swatch input[type=radio]").click( function() {
	var id = jQuery(this).attr("attr-id");
jQuery('.feed-img-disable' ).show();
jQuery('.feed-img-enable' ).hide();

jQuery('#dis-img'+id ).hide();
jQuery('#enb-img'+id ).show();
if(id == 3) {
  jQuery('#likeit-text').hide();
 jQuery('#no-text').show();
}
else
{
 jQuery('#likeit-text').show();
 jQuery('#no-text').hide();
}
});

</script>
<script>
jQuery(document).ready(function(){
  jQuery(".chk").click(function() { jQuery(".change_title").show(); });
});
</script>

<script>

jQuery(function(){
jQuery(".submit-feedback").click(function(e){
e.preventDefault();
var message = jQuery("#feed-message").val();
var email= jQuery("#feed-email").val();
if(email == ""){
jQuery("#feed-email").css("border","1px solid red");
return false;
}
var chke = jQuery('input[name=feed-name]:checked').val();
jQuery.ajax({
method: "POST",
url: "http://ladyraga.devskart.com/wp-content/themes/ladyraga/feed-ajax.php",
data: { message:message ,chke:chke, email:email}
})
.done(function( msg ) {
//alert(msg );
jQuery('.feedback_form').html(msg);

});
});
});

</script>
<!--------------------------------12 june-------------------------------->
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery(".swatch").find("input[type='checkbox']").change(function(){
    if(jQuery(this).is(":checked")){
        jQuery(this).parent().addClass("swatch_red"); 
    }else{
        jQuery(this).parent().removeClass("swatch_red");  
    }
});
});
</script>
<!---------------------------13 june---------------------------->
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#raf-message").find(".btn").click(function(){
      jQuery(this).addClass("raf-btn");
      jQuery("input#foo").addClass("raf-highlights");
});
});
</script>

<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('#raf-message > .btn').prepend('<i class="fa fa-clipboard" aria-hidden="true"></i>');
});
</script>



</body>
</html>
