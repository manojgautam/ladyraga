<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<div class="row" id="customer_details">
	<div class="col-md-6 col-md-offset-3 checkout-login">
		<h2><span class="light">Please <?php _e( 'Login', 'woocommerce' ); ?></span></h2>		<hr class="colorgraph">

		<form method="post" class="login">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
				<label for="username"><?php _e( 'Username or email address', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
			</p>
			<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
				<label for="password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
			</p>

			<?php do_action( 'woocommerce_login_form' ); ?>

			<div class="form-row inline">			<div class="col-sm-7 nopadding">
				<?php wp_nonce_field( 'woocommerce-login' ); ?>
				<?php
					thegem_button(array(
						'tag' => 'button',
						'text' => __( 'Login', 'woocommerce' ),
						'style' => 'outline',
						'size' => 'medium',
						'extra_class' => 'checkout-login-button',
						'attributes' => array(
							'type' => 'submit',
							'name' => 'login',
							'value' => __( 'Login', 'woocommerce' )
						)
					), true);
				?>				<div class="checkboxs">				<span class="checkout-login-remember">					<input name="rememberme" type="checkbox" id="rememberme" value="forever" class="gem-checkbox" />					<label for="rememberme" class="inline"> <?php _e( 'Remember me', 'woocommerce' ); ?></label>				</span>				</div>				</div>				<div class="woocommerce-LostPassword lost_password col-sm-5">				<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a>			</div>				
			</div>						<div class="or">			<span>OR</span>			</div>						<div class="gem-button-container gem-button-position-inline get-started-button">			<a href="/get-started" class="gem-button gem-button-size-medium gem-button-style-outline gem-button-text-weight-normal gem-button-border-2" style="border-radius: 3px;">	Get Started</a>			</div>
			

			<?php do_action( 'woocommerce_login_form_end' ); ?>
				
		</form>
	</div>
</div>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
