  <?php
/**
 * Template Name: Get Start Page
 *
 * @package TheGem
 */

get_header(); ?>

<div id="main-content" class="main-content">

<?php echo thegem_page_title(); ?>


<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
<div class="register-get">
<div class="container">

  <form role="form" id="myForm"  data-toggle="validator" action="" onsubmit="return checkfunction();" method="post">
  	<?php do_action( 'woocommerce_register_form_start' ); ?>
            <div id="smartwizard">
            <ul>
                <li><a href="#step-1"></a></li>
                <li><a href="#step-2"></a></li>
                <li><a href="#step-3"></a></li>
                <li><a href="#step-4"></a></li>
				<li><a href="#step-5"></a></li>
                <li><a href="#step-6"></a></li>
                <li><a href="#step-7"></a></li>
                <li><a href="#step-8"></a></li>
				<li><a href="#step-9"></a></li>
				<li><a href="#step-10"></a></li>
				
            </ul>
            
            <div>
			<span id="myModal" class="modal">

				  <!-- Modal content -->
				  <span class="modal-content">
					<span class="close">&times;</span>
					<p>Please Select the element</p>
				  </span>

				</span>
                <div id="step-1">
                     <div id="form-step-0" role="form" data-toggle="validator" >
                        <div class="form-group has-error has-danger">
                           	<h3>Skin Tone That Matches Yours ?</h3>
								<div  class="skin-tone">
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_01/fair.png);" class="swatch skin-1">
								<input type="radio" class="check-0" attr-id="0" name="skin-tone" value="Fair"> 
								</div>
								<div class="label">Fair</div>
								</div>

								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_01/light.png);" class="swatch skin-2">
								<input type="radio" class="check-0" attr-id="0" name="skin-tone" value="Light"> 
								</div>
								<div class="label">Light</div>
								</div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_01/medium.png);" class="swatch skin-3">
								<input type="radio" class="check-0" attr-id="0" name="skin-tone" value="Medium">
								</div>
								<div class="label"> Medium</div>
								</div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_01/tan.png);" class="swatch skin-4">
								<input type="radio" class="check-0" attr-id="0" name="skin-tone" value="Tan/Olive">
								</div>
								<div class="label"> Tan / Olive</div>
								</div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_01/dark.png);" class="swatch skin-5">
								<input type="radio" class="check-0" attr-id="0" name="skin-tone" value="Dark">
								</div>
								<div class="label"> Dark</div>
								</div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_01/deep.png);" class="swatch skin-6">
								<input type="radio" class="check-0" attr-id="0" name="skin-tone" value="Deep">
								</div>
								<div class="label"> Deep  </div>
								</div>
								</div>
						</div>
                    </div>
                    
                </div>
                <div id="step-2">
                    <div id="form-step-1" role="form" data-toggle="validator" novalidate="true">
                        <div class="form-group has-error has-danger">
							<h3>Skin Type That Matches Yours?</h3>
							  <div  class="skin-tone">
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_02/brown.png);" class="swatch Brown">
								<input type="radio" class="check-1" attr-id="1" name="skin-type" value="Brown">
								</div>
								<div class="label">Brown</div>
								</div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_02/hazel.png);" class="swatch Hazel">
								<input type="radio" class="check-1" attr-id="1" name="skin-type" value="Hazel"></div>	
								<div class="label">Hazel</div>
								</div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_02/blue.png);" class="swatch Blue">
								<input type="radio" class="check-1" attr-id="1" name="skin-type" value="Blue">
								</div>
								<div class="label"> Blue</div>
								</div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_02/green.png);" class="swatch Green">
								<input type="radio" class="check-1" attr-id="1" name="skin-type" value="Green"></div>
								<div class="label">Green</div></div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_02/amber.png);" class="swatch Amber">
								<input type="radio" class="check-1" attr-id="1" name="skin-type" value="Amber"></div>
								<div class="label">Amber</div></div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_02/gray.png);" class="swatch Gray">
								<input type="radio" class="check-1" attr-id="1" name="skin-type" value="Gray"></div>
								<div class="label">Gray</div></div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_02/violet.png);" class="swatch Violet">
								<input type="radio" class="check-1" attr-id="1" name="skin-type" value="Violet"></div>
								<div class="label">Violet</div></div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_02/other.png);" class="swatch Other">
								<input type="radio" class="check-1" attr-id="1" name="skin-type" value="Other"></div>
								<div class="label">Other</div></div>
							 </div>	
                        </div>
                    </div>
                </div>
                <div id="step-3">
                    <div id="form-step-2" role="form" data-toggle="validator">
                        <div class="form-group">
                            <h3>Hair Type That Matches Yours </h3>
							  <div  class="skin-tone">
							  <div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_03/silky.png);" class="swatch Black">
								<input type="radio" class="check-2" attr-id="2" name="hair" value="Silky-Straight"></div> 
								<div class="label">Silky Straight</div></div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_03/body.png);" class="swatch Dark-Brown">
								<input type="radio" class="check-2" attr-id="2" name="hair" value="Body-Wave"></div>
								<div class="label">Body Wave</div></div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_03/loose.png);" class="swatch Light-Brown">
								<input type="radio" class="check-2" attr-id="2" name="hair" value="Loose-Curl"></div>
								<div class="label">Loose Curl</div></div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_03/kinky.png);" class="swatch Blonde">
								<input type="radio" class="check-2" attr-id="2" name="hair" value="Kinky-Curl"></div>
								<div class="label">Kinky Curl</div></div>
								</div>
                        </div>
                    </div>
                </div>
				
				<div id="step-4">
                    <div id="form-step-3" role="form" data-toggle="validator">
                        <div class="form-group">
                            <h3>Your Comfort Level With Trying Different Nail Colors</h3>
								<div  class="skin-tone">
								<div class="flat_bg col-sm-4">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_04/nail_v1.png">
								<input type="radio" class="check-3" attr-id="3" name="comfrt-nail" value="Regular-Shades">
								</div> 
								<div class="label">
								<h3>Regular Shades</h3> 
								<p>I rarely or never wear makeup because I don't know how to apply much of it.</p>
								</div>
								</div>
								<div class="flat_bg col-sm-4">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_04/nail_v2.png">
								<input type="radio" class="check-3" attr-id="3" name="comfrt-nail" value="Shades-In-Trend">
								</div> 
								<div class="label">
								<h3>Shades In Trend</h3> 
								<p>I like makeup, but I am looking to learn more about what to wear and how to wear it.</p>
								</div>
								</div>
								<div class="flat_bg col-sm-4">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_04/nail_v3.png">
								<input type="radio" class="check-3" attr-id="3" name="comfrt-nail" value="Try-New-Shades">
								</div>
								<div class="label"> 
								<h3>Try New Shades</h3> 
								<p>I am a makeup pro and I never leave the house without looking glamorous!</p>
								</div>
								</div>
								</div>
                        </div>
                    </div>
                </div>
				
				<div id="step-5">
                    <div id="form-step-4" role="form" data-toggle="validator">
                        <div class="form-group">
                            <h3>Your Comfort Level With Trying Different Lip Colors </h3>
								<div  class="skin-tone">
								<div class="flat_bg col-sm-4">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_05/lip_01.png">
								<input type="radio" class="check-4" attr-id="4" name="comfrt-lip" value="Regular-Shades">
								</div> 
								<div class="label">
								<h3>Regular Shades</h3> 
								<p>I rarely or never wear makeup because I don't know how to apply much of it.</p>
								</div>
								</div>
								<div class="flat_bg col-sm-4">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_05/lip_02.png">
								<input type="radio" class="check-4" attr-id="4" name="comfrt-lip" value="Shades-In-Trend">
								</div> 
								<div class="label">
								<h3>Shades In Trend</h3> 
								<p>I like makeup, but I am looking to learn more about what to wear and how to wear it.</p>
								</div>
								</div>
								<div class="flat_bg col-sm-4">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_05/lip_03.png">
								<input type="radio" class="check-4" attr-id="4" name="comfrt-lip" value="Try-New-Shades">
								</div>
								<div class="label"> 
								<h3>Try New Shades</h3> 
								<p>I am a makeup pro and I never leave the house without looking glamorous!</p>
								</div>
								</div>
								</div>
                        </div>
                    </div>
                </div>
				
						
				 <div id="step-6">
                    <div id="form-step-5" role="form" data-toggle="validator">
                        <div class="form-group">
                            <h3>Your Comfort Level With Trying Different Eye Makeup </h3>
							  <div  class="skin-tone">
							  <div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_06/eye_01.png);" class="swatch Black">
								<input type="radio" class="check-5" attr-id="5" name="comfrt-eye" value="Black-Is-Mine"></div> 
								<div class="label">Black Is Mine</div></div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_06/eye_02.png);" class="swatch Dark-Brown">
								<input type="radio" class="check-5" attr-id="5" name="comfrt-eye" value="Sober-Shades"></div>
								<div class="label">Sober Shades</div></div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_06/eye_03.png);" class="swatch Light-Brown">
								<input type="radio" class="check-5" attr-id="5" name="comfrt-eye" value="Shades-In-Trend"></div>
								<div class="label">Shades In Trend</div></div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_06/eye_04.png);" class="swatch Blonde">
								<input type="radio" class="check-5" attr-id="5" name="comfrt-eye" value="Love-To-Try-New-Shades
"></div>
								<div class="label">Love To Try New Shades</div></div>
								</div>
                        </div>
                    </div>
                </div>
				<div id="step-7">
                    <div id="form-step-6" role="form" data-toggle="validator">
                        <div class="form-group">
                            <h3>How Much You Are Comfortable With Makeup? </h3>
							  <div  class="skin-tone">
							  <div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_07/01.png);" class="swatch Black">
								<input type="radio" class="check-6" attr-id="6" name="comfrt-makeup" value="Not-comfortable"></div> 
								<div class="label">Not Much Comfortable [I rarely use makeup]</div></div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_07/02.png);" class="swatch Dark-Brown">
								<input type="radio" class="check-6" attr-id="6" name="comfrt-makeup" value="Bit-Comfortable"></div>
								<div class="label">Bit Comfortable [I like makeup but hardly get time]</div></div>
								<div  class="choice">
								<div style="background:url(/wp-content/themes/ladyraga/images/getstarted/page_07/03.png);" class="swatch Light-Brown">
								<input type="radio" class="check-6" attr-id="6" name="comfrt-makeup" value="Feel-Good"></div>
								<div class="label">Feels Good [I am makeup freak & never leaves home without having a dose of makeup]</div></div>
								</div>
                        </div>
                    </div>
                </div>
				
				<div id="step-8">
                    <div id="form-step-7" role="form" data-toggle="validator">
                        <div class="form-group">
                        <h3>What’s Your Style of Imitation Jewellery? </h3>
							<div  class="skin-tone beauty_brands">
							 	<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_08/jwell_01.png">
								<div class="label">
									<p>Ethnic</p>
								</div>
								<input type="checkbox" name="brand[]" value="Ethnic" class="addchkbox check-7">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_08/jwell_02.png">
								<div class="label">
									<p>Traditional</p>
								</div>
								<input type="checkbox" name="brand[]" value="Traditional" class="addchkbox check-7">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_08/jwell_03.png">
								<div class="label">
									<p>Fashion</p>
								</div>
								<input type="checkbox" name="brand[]" value="Fashion" class="addchkbox check-7">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_08/jwell_04.png">
								<div class="label">
									<p>Regular In Trend</p>
								</div>
								<input type="checkbox" name="brand[]" value="Regular-In-Trend" class="addchkbox check-7">
								<label></label>
								</div>
								</div>	
							
							</div>
                        </div>
                    </div>
                </div>
				
				<div id="step-9">
                    <div id="form-step-8" role="form" data-toggle="validator">
                        <div class="form-group">
                             <h3>Which of these types of makeup products do you love the most?</h3>	
							 <div  class="skin-tone beauty_brands">
							 	<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_09/product_01.png">
								<div class="label">
								<p>Sling Bags</p>
								</div>
								<input type="checkbox" name="makeup[]" value="Sling-Bags" class="addchkbox check-8">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_09/product_02.png">
								<div class="label">
								<p>Clutch Bags</p>
								</div>
								<input type="checkbox" name="makeup[]" value="Clutch-Bags" class="addchkbox check-8">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_09/product_03.png">
								<div class="label">
								<p>Shades</p>
								</div>
								<input type="checkbox" name="makeup[]" value="Shades" class="addchkbox check-8">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_09/product_04.png">
								<div class="label">
								<p>Scarfs</p>
								</div>
								<input type="checkbox" name="makeup[]" value="Scarfs" class="addchkbox check-8">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_09/product_05.png">
								<div class="label">
								<p>Belts</p>
								</div>
								<input type="checkbox" name="makeup[]" value="Belts" class="addchkbox check-8">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_09/product_06.png">
								<div class="label">
								<p>Caps</p>
								</div>
								<input type="checkbox" name="makeup[]" value="Caps" class="addchkbox check-8">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_09/product_07.png">
								<div class="label">
								<p>Rings</p>
								</div>
								<input type="checkbox" name="makeup[]" value="Rings" class="addchkbox check-8">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_09/product_08.png">
								<div class="label">
								<p>Necklace</p>
								</div>
								<input type="checkbox" name="makeup[]" value="Necklace" class="addchkbox check-8">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_09/product_09.png">
								<div class="label">
								<p>Pendants</p>
								</div>
								<input type="checkbox" name="makeup[]" value="Pendants" class="addchkbox check-8">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_09/product_10.png">
								<div class="label">
								<p>Brooch</p>
								</div>
								<input type="checkbox" name="makeup[]" value="Brooch" class="addchkbox check-8">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_09/product_11.png">
								<div class="label">
								<p>Bracelet</p>
								</div>
								<input type="checkbox" name="makeup[]" value="Bracelet" class="addchkbox check-8">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_09/product_12.png">
								<div class="label">
								<p>Hair Bands</p>
								</div>
								<input type="checkbox" name="makeup[]" value="Hair-Bands" class="addchkbox check-8">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_09/product_13.png">
								<div class="label">
								<p>Hair Clips</p>
								</div>
								<input type="checkbox" name="makeup[]" value="Hair-Clips" class="addchkbox check-8">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_09/product_14.png">
								<div class="label">
								<p>Anklets</p>
								</div>
								<input type="checkbox" name="makeup[]" value="Anklets" class="addchkbox check-8">
								<label></label>
								</div>
								</div>
								<div class="flat_bg col-sm-2">
								<div class="icon swatch">
								<img src="/wp-content/themes/ladyraga/images/getstarted/page_09/product_15.png">
								<div class="label">
								<p>Trendy Watches</p>
								</div>
								<input type="checkbox" name="makeup[]" value="Trendy-Watches" class="addchkbox check-8">
								<label></label>
								</div>
								</div>
							
							</div>
						  </div>
                    </div>
                </div>
				
	
                <div id="step-10" class="">
                      <div id="form-step-9" role="form" data-toggle="validator">
                        <div class="form-group">
                  		<div class="register-inner">
										
		

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="reg_username"><?php _e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
				</p>

			<?php endif; ?>

			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="reg_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
			</p>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="reg_password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" />
				</p>

			<?php endif; ?>

			<!-- Spam Trap -->
			<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" autocomplete="off" /></div>

			<?php do_action( 'woocommerce_register_form' ); ?>

			<p class="woocomerce-FormRow form-row">
				<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
				<input type="submit" class="woocommerce-Button button" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>" />
			</p>

			<?php do_action( 'woocommerce_register_form_end' ); ?>

					</div>

	                      </div>
                    </div>
                    
                    
                </div>
            </div>
        </div>
        
  </form>
  </div>
</div>

</div><!-- #main-content -->
<?php endif; ?>

<?php
get_footer();
?>

    <script type="text/javascript">
        jQuery(document).ready(function(){
            
            // Toolbar extra buttons
            var btnFinish = jQuery('<button></button>').text('Finish')
                                             .addClass('btn btn-info')
                                             .on('click', function(){ 
                                                    if( !$(this).hasClass('disabled')){ 
                                                        var elmForm = $("#myForm");
                                                        if(elmForm){
                                                            elmForm.validator('validate'); 
                                                            var elmErr = elmForm.find('.has-error');
                                                            if(elmErr && elmErr.length > 0){
                                                                alert('Oops we still have error in the form');
                                                                return false;    
                                                            }else{
                                                                alert('Great! we are ready to submit form');
                                                                elmForm.submit();
                                                                return false;
                                                            }
                                                        }
                                                    }
                                                });
            var btnCancel = jQuery('<button></button>').text('Cancel')
                                             .addClass('btn btn-danger')
                                             .on('click', function(){ 
                                                    jQuery('#smartwizard').smartWizard("reset"); 
                                                    jQuery('#myForm').find("input, textarea").val(""); 
                                                });                         
            
            
            
            // Smart Wizard
            jQuery('#smartwizard').smartWizard({ 
                    selected: 0, 
                    theme: 'dots',
                    transitionEffect:'fade',
                    toolbarSettings: {toolbarPosition: 'bottom',
                                      toolbarExtraButtons: [btnFinish, btnCancel]
                                    },
                    anchorSettings: {
                                markDoneStep: true, // add done css
                                markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                                removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                                enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                            }
                 });
            
            jQuery("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
                var elmForm = jQuery("#form-step-" + stepNumber);
				var modal = document.getElementById('myModal');
				
                // stepDirection === 'forward' :- this condition allows to do the form validation 
                // only on forward navigation, that makes easy navigation on backwards still do the validation when going next
                if(stepDirection === 'forward' && elmForm){
                    elmForm.validator('validate'); 
                    var elmErr = elmForm.children('.has-error');	
                    if(elmErr && elmErr.length > 0){
                        // Form validation failed
                        return false;    
                    }
					var chclas = false; 
					jQuery(".check-"+stepNumber).each(function(){
						// console.log(jQuery("check-"+stepNumber).is(':checked'));
						if(jQuery(this).is(':checked')){
							chclas = true;
						}
					});
					if(!chclas){
						jQuery("#form-step-" + stepNumber+" .form-group").addClass("has-error");
					}
					var check = jQuery("#form-step-" + stepNumber+" .form-group").hasClass("has-error");
					//console.log(check);
					if(check){
					//	modal.style.display = "block";
					 //  return false;
					}
                }
				jQuery('.err-msg').html("");
                return true;
            });
			var modal = document.getElementById('myModal');
			var span = document.getElementsByClassName("close")[0];
			span.onclick = function() {
				modal.style.display = "none";
			}
            
            jQuery("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
                // Enable finish button only on last step
                if(stepNumber == 9){ 
				
                    jQuery('.btn-finish').removeClass('disabled');
                    
					
                }else{
					 
                    jQuery('.btn-finish').addClass('disabled');
                    
                  
                }
            });                               
            
        });   
    </script>  
	<script>
	jQuery(".swatch input[type=radio]").click( function() {
		var id = jQuery(this).attr("attr-id");
		jQuery(".check-"+id).parent().removeClass("selectedTone");
		jQuery(this).parent().addClass("selectedTone");
	});
	
	</script>
	
	<script>
	jQuery('.addchkbox').change(function(){
		
    if(jQuery(this).is(":checked")) {
        jQuery(this).addClass("checked");
    } else {
		
        jQuery(this).removeClass("checked");
    }
});
</script>
		<script>
		var check =1;
	
function checkfunction(){
	var email = jQuery("#re_email").val();
	if(check == 1){
		jQuery.ajax({
			method: "POST",
			url: "http://ladyraga.devskart.com/ajax.php",
			data: { 'email' :email},
			success: function(data){
				if(data==''){ 
					check = 2;
					jQuery('#Registerationbtn').click();
				}else{
					jQuery(".error-email").text("Email already exist!");
				}
			}
		});
		return false;
	}else{
		return true;
	}
	
	
}
	</script>
	<script>
	 jQuery(document).ready(function(){
		 jQuery(".sw-btn-group-extra").hide();
		 
	 });
	</script>
<!-- hide next/prev button on last step -->
<script>
jQuery(document).ready(function(){
jQuery( ".sw-btn-next" ).click(function() {
var prop = jQuery("#step-9").css('display');
if(prop == "block"){
var check = jQuery("#form-step-8 .form-group").hasClass("has-error");

if(!check){
jQuery(".sw-toolbar-bottom").hide();
}
}
});
jQuery( ".step-anchor" ).click(function() {
jQuery(".sw-toolbar-bottom").show();
});

});

</script>

