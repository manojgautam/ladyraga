<?php
/**
 * Template Name: Subscribe Page
 *
 * @package TheGem
 */

$thegem_page_data = get_post_meta(get_the_ID(), 'thegem_page_data', TRUE);
$thegem_slideshow_params = array_merge(array('slideshow_type' => '', 'slideshow_slideshow' => '', 'slideshow_layerslider' => '', 'slideshow_revslider' => ''), $thegem_page_data);

get_header(); ?>

<div id="main-content" class="main-content">


<?php
	if($thegem_slideshow_params['slideshow_type']) {
		thegem_slideshow_block(array('slideshow_type' => $thegem_slideshow_params['slideshow_type'], 'slideshow' => $thegem_slideshow_params['slideshow_slideshow'], 'lslider' => $thegem_slideshow_params['slideshow_layerslider'], 'slider' => $thegem_slideshow_params['slideshow_revslider']));
	}
?>
<?php //echo thegem_page_title(); ?>
<?php
	while ( have_posts() ) : the_post();
		get_template_part( 'content', 'page' );
	endwhile;
?>
<div class="container">

<div class="pricing-table row inline-row inline-row-center pricing-table-style-1 button-icon-default">
    <?php
        $args = array( 'post_type' => 'product', 'posts_per_page' => 4, 'product_cat' => 'monthly-subscriptions', 'orderby' => 'ASC' );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>

            

               <div class="pricing-column-wrapper col-md-3 col-sm-4 col-xs-6  inline-column">
				<div class="pricing-column">

                 

                        <?php woocommerce_show_product_sale_flash( $post, $product ); ?>

                         <?php // if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />'; ?> 
					 <div class="pricing-price-row">
						<div class="pricing-price-title-wrapper">
						   <div class="pricing-price-title"><?php the_title(); ?></div>
						   <div class="pricing-price-subtitle"></div>
						</div>
						<div class="pricing-price-wrapper">
						   <div class="pricing-price" style="">
							  <div style=" " class="pricing-cost"><?php echo $product->get_price_html(); ?></div>
						   </div>
						</div>
					 </div>
					 	 <div class="pricing-footer">
						<div class="gem-button-container gem-button-position-center">
						<div class="gem-button gem-button-size-small gem-button-style-flat gem-button-text-weight-normal gem-button-icon-position-left"> <?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?></div>
						</div>
					 </div>    
					<?php the_content(); ?>
					
									
         

             
                </div>
                </div>
				
				

    <?php endwhile; ?>
    <?php wp_reset_query(); ?>
</div><!--/.products-->

</div>

</div>
<div class="clearfix"></div>
<?php
get_footer();
