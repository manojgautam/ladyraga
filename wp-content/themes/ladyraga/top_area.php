<div id="top-area" class="top-area top-area-style-default top-area-alignment-<?php echo esc_attr(thegem_get_option('top_area_alignment', 'left')); ?>">
	<div class="container">
		<div class="top-area-items inline-inside">
		<?php if(thegem_get_option('top_area_socials')) : ?>
				<div class="top-area-block top-area-socials<?php echo esc_attr(thegem_get_option('top_area_style') == 1 ? ' socials-colored-hover' : ''); ?>"><?php thegem_print_socials(); ?></div>
			<?php endif; ?>
			<div class="top-area-block stor-surprise">
			<div class="store">Store of Surprises</div>
			  </div>
			<?php if(thegem_get_option('top_area_contacts')) : ?>
				<div class="top-area-block top-area-contacts"><?php echo thegem_top_area_contacts(); ?></div>
			<?php endif; ?>
			<?php if(has_nav_menu('top_area') || thegem_get_option('top_area_button_text')) : ?>
				<div class="top-area-block top-area-menu">
					<?php if(has_nav_menu('top_area')) : ?>
						<nav id="top-area-menu">
							<?php wp_nav_menu(array('theme_location' => 'top_area', 'menu_id' => 'top-area-navigation', 'depth' => 1, 'menu_class' => 'nav-menu styled inline-inside', 'container' => false, 'walker' => new thegem_walker_footer_nav_menu)); ?>
						</nav>
					<?php endif; ?>
					<?php if(thegem_get_option('top_area_button_text')) : ?>
						<div class="top-area-button"><?php thegem_button(array('href' => thegem_get_option('top_area_button_link'), 'text' => thegem_get_option('top_area_button_text'), 'size' => 'tiny', 'no-uppercase' => true), true); ?></div>
					<?php endif; ?>
				</div>
				
			<?php endif; ?>
			<div class="top-area-block curreny-switcher">
					<?php echo do_shortcode(' [woocs width=’50%’] ') ; ?>
					</div>
					<div class="top-area-block favourite_popup">
					<div class="callus">
<a href="#modal"><i class="fa fa-heart" aria-hidden="true"></i></a>
</div>
					</div>

<!----------Call-Us-popup------------->
<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
<!-- <?php //echo do_shortcode('[contact-form-7 id="25292" title="Feedback"]'); ?>
<span id=show_res> </span> -->
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
<div class="feedback_form">
<h3>Love our new upgrade?</h3>
<div class="select_below">
<div class="flat_bg">
<div class="icon swatch">
								<img src="<?php echo get_template_directory_uri(); ?>/images/smily/like_it_disable.png" id="dis-img1"  class="feed-img-disable">
								<img src="<?php echo get_template_directory_uri(); ?>/images/smily/like_it_enable.png" id="enb-img1" style="display:none;" class="feed-img-enable">
								

								<input type="radio" class="like-it chk" attr-id="1" name="feed-name" value="Like-it">
								</div>
								</div>
								
								<div class="flat_bg">
								<div class="icon swatch">
								<img src="<?php echo get_template_directory_uri(); ?>/images/smily/maybe_disable.png" id="dis-img2"class="feed-img-disable">
								<img src="<?php echo get_template_directory_uri(); ?>/images/smily/maybe_enable.png" id="enb-img2" style="display:none;" class="feed-img-enable">
								<input type="radio" class="maybe chk" attr-id="2" name="feed-name" value="Maybe">
								</div>
								</div>
								
								<div class="flat_bg">
								<div class="icon swatch">
								<img src="<?php echo get_template_directory_uri(); ?>/images/smily/no_disable.png" id="dis-img3" class="feed-img-disable">
								<img src="<?php echo get_template_directory_uri(); ?>/images/smily/no_enable.png" id="enb-img3" style="display:none;" class="feed-img-enable">
								<input type="radio" class="no chk" attr-id="3" name="feed-name" value="No">
								</div>
								</div>
								
								
</div>
<div class="change_title">
<div id="likeit-text">Awesome, tell us more!</div>
<div id="no-text">Tell us what went wrong</div>
<textarea name="your-message" id="feed-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Write a comment (optional)"></textarea><br>
<input type="email" name="email" placeholder="Enter your Email" class="feed-email" id="feed-email">
<div class="btn-sub"><input type="submit" value="submit" class="submit-feedback"></div>
</div>

</div>
</form>
</div>


<!--call-us----->
		
		</div>
	</div>
</div>
