<?php
// hide specific monthly subscription category's Products shop page
add_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

function custom_pre_get_posts_query( $q ) {

	if ( ! $q->is_main_query() ) return;
	if ( ! $q->is_post_type_archive() ) return;
	
	if ( ! is_admin() && is_shop() ) {

		$q->set( 'tax_query', array(array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => array( 'monthly-subscriptions' ), // Don't display products in the knives category on the shop page
			'operator' => 'NOT IN'
		)));
	
	}

	remove_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

}
// hide specific category from shop page
      add_filter( 'get_terms', 'get_subcategory_terms', 10, 3 );

    function get_subcategory_terms( $terms, $taxonomies, $args ) {

    $new_terms = array();

    // if a product category and on the shop page/archive page/single product page
    if ( in_array( 'product_cat', $taxonomies ) && ! is_admin() && is_shop() || in_array( 'product_cat', $taxonomies ) && ! is_admin() && is_archive() || in_array( 'product_cat', $taxonomies ) && ! is_admin() && is_single() ) {

    foreach ( $terms as $key => $term ) {

    if ( ! in_array( $term->slug, array( 'monthly-subscriptions' ) ) ) {
    $new_terms[] = $term;
    }

    }

    $terms = $new_terms;
    }

    return $terms;
    }

	
// Hook in change order note text and placeholder
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
  
// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     $fields['order']['order_comments']['label'] = 'Order Notes';
     $fields['order']['order_comments']['placeholder'] = 'Notes about your order, e.g. special notes for delivery.';
     return $fields;
}

// Stop plugin updates
remove_action('load-update-core.php','wp_update_plugins');
add_filter('pre_site_transient_update_plugins','__return_null');



function storefront_child_remove_checkout_fields($fields) { 
    unset( $fields ['billing'] ['billing_company'] ); 
    return $fields; 
}

add_filter( 'woocommerce_checkout_fields', 'storefront_child_remove_checkout_fields' );

function wpse215677_checkout_fields ( $fields ) {
    $fields['billing_postcode']['maxlength'] = 6;
     $fields['billing_phone']['maxlength'] = 10;  

    return $fields;
}
add_filter('woocommerce_billing_fields', 'wpse215677_checkout_fields');

function bbloomer_gateway_disable_shipping( $available_gateways ) {
//echo "<pre>";
//print_r($available_gateways);
global $woocommerce;
//echo "<pre>";
//print_r($woocommerce);
//echo $woocommerce->total;
 $amount2 = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_cart_total() ) );
 if($amount2 >='500'){
$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
 $chosen_shipping = $chosen_methods[0];
if ( isset( $available_gateways['payuindia'] ) && $chosen_shipping == 'flat_rate:2' ) {
unset( $available_gateways['payuindia'] );
unset( $available_gateways['paytm'] );
unset( $available_gateways['ppec_paypal'] );
}
}
return $available_gateways;
}
 add_filter( 'woocommerce_available_payment_gateways', 'bbloomer_gateway_disable_shipping' );

  
add_filter( 'woocommerce_package_rates', 'bbloomer_unset_shipping_when_free_is_available_in_zone', 10, 2 );
  
function bbloomer_unset_shipping_when_free_is_available_in_zone( $rates, $package ) {
     
    // Only unset rates if free_shipping is available
    if ( isset( $rates['free_shipping:4'] ) ) {
    unset( $rates['flat_rate:7'] );
}elseif ( isset( $rates['flat_rate:7'] ) ) {
    unset( $rates['flat_rate:2'] );
}
   return $rates;
}