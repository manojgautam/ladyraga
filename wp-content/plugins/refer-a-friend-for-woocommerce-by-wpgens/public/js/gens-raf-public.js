(function( $ ) {
	'use strict';

	//Javascript GET cookie parameter
	var $_GET = {};
	document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
	    function decode(s) {
	        return decodeURIComponent(s.split("+").join(" "));
	    }

	    $_GET[decode(arguments[1])] = decode(arguments[2]);
	});

	// Get time var defined in woo backend
	var $time = parseInt(gens_raf.timee);
	//If raf is set, add cookie.
	if( typeof $_GET["raf"] !== 'undefined' && $_GET["raf"] !== null ){
		//console.log(window.location.hostname);
		cookie.set("gens_raf",$_GET["raf"],{ expires: $time, path:'/' });
	}

})( jQuery );

function ss_plugin_loadpopup_js(em){
	var shareurl=em.href;
	var top = (screen.availHeight - 500) / 2;
	var left = (screen.availWidth - 500) / 2;
	var popup = window.open(
			shareurl, 
			'social sharing',
			'width=550,height=420,left='+ left +',top='+ top +',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1'
	);
	return false;
}