<?php
/**
 * Plugin Name: Get Started Users
 * Plugin URI: 
 * Description: This plugin allows admin to put postcode  page 
 * Version: 1.0.0
 * Author: Naveen Kumar
 * Author URI: 
 * License: GPLv2
 */
//define('WP_DEBUG',true); 
function theme_name_scripts() {
       //wp_enqueue_style( 'style-name', plugin_dir_url(__FILE__) .'js/jquery.dataTables.min.css');
	wp_register_script('test', plugin_dir_url(__FILE__) . 'js/jquery.dataTables.min.js', array('jquery'), '1.0', true);
}
add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );

register_activation_hook(__FILE__, 'plugin_name_activation');
add_action( 'admin_menu', 'register_my_custom_menu_page' );



function register_my_custom_menu_page() {
    add_menu_page("custom menu title", "Get Started Users", 0, "my-menu-slug", "my_custom_menu_page");
   // add_submenu_page("my-menu-slug", "My Submenu", "Subscribe Users", 0, "my-submenu-slug", "subscribe_list");
}

function subscribe_list(){
    global $wpdb;
    		global $wpdb;
	 $table_name1= $wpdb->prefix .'pmpro_memberships_users';
	 
	 $sqlQuery = "SELECT SQL_CALC_FOUND_ROWS u.ID, u.user_login, u.user_email, UNIX_TIMESTAMP(u.user_registered) as joindate, mu.membership_id, mu.initial_payment, mu.billing_amount, mu.cycle_period, mu.cycle_number, mu.billing_limit, mu.trial_amount, mu.trial_limit, UNIX_TIMESTAMP(mu.startdate) as startdate, UNIX_TIMESTAMP(mu.enddate) as enddate, m.name as membership FROM $wpdb->users u LEFT JOIN $wpdb->pmpro_memberships_users mu ON u.ID = mu.user_id LEFT JOIN $wpdb->pmpro_membership_levels m ON mu.membership_id = m.id";
//	$sql2="SELECT SQL_CALC_FOUND_ROWS u.ID, u.user_login, u.user_email, UNIX_TIMESTAMP(u.user_registered) as joindate, mu.membership_id, mu.initial_payment, mu.billing_amount, mu.cycle_period, mu.cycle_number, mu.billing_limit, mu.trial_amount, mu.trial_limit, UNIX_TIMESTAMP(mu.startdate) as startdate, UNIX_TIMESTAMP(mu.enddate) as enddate, m.name as membership FROM $wpdb->users u LEFT JOIN $wpdb->usermeta um ON u.ID = um.user_id LEFT JOIN $wpdb->pmpro_memberships_users mu ON u.ID = mu.user_id LEFT JOIN $wpdb->pmpro_membership_levels m ON mu.membership_id = m.id ";
                 $resuth1=$wpdb->get_results($sqlQuery);
    
   // $sqlQuery = "SELECT SQL_CALC_FOUND_ROWS u.ID, u.user_login, u.user_email, UNIX_TIMESTAMP(u.user_registered) as joindate, mu.membership_id, mu.initial_payment, mu.billing_amount, mu.cycle_period, mu.cycle_number, mu.billing_limit, mu.trial_amount, mu.trial_limit, UNIX_TIMESTAMP(mu.startdate) as startdate, UNIX_TIMESTAMP(mu.enddate) as enddate, m.name as membership FROM $wpdb->users u LEFT JOIN $wpdb->usermeta um ON u.ID = um.user_id LEFT JOIN $wpdb->pmpro_memberships_users mu ON u.ID = mu.user_id LEFT JOIN $wpdb->pmpro_membership_levels m ON mu.membership_id = m.id ";
   // $resut=$wpdb->get_results($sqlQuery);
   // print($resuth1);
     ?>
	<table class="widefat">
		<thead>
			<tr class="thead">
				<th><?php _e('ID', 'pmpro');?></th>
				<th><?php _e('Username', 'pmpro');?></th>
				<th><?php _e('First&nbsp;Name', 'pmpro');?></th>
				<th><?php _e('Last&nbsp;Name', 'pmpro');?></th>
				<th><?php _e('Email', 'pmpro');?></th>
                                <th><?php _e('Invoice IDs'); ?></th>
                                <th><?php _e('Order IDs'); ?></th>
				<?php do_action("pmpro_memberslist_extra_cols_header", $theusers);?>
				<th><?php _e('Billing Address', 'pmpro');?></th>
				<th><?php _e('Membership', 'pmpro');?></th>
				<th><?php _e('Fee', 'pmpro');?></th>
				<th><?php _e('Joined', 'pmpro');?></th>
				<th>
					<?php
						if($l == "oldmembers")
							_e('Ended', 'pmpro');
						else
							_e('Expires', 'pmpro');
					?>
				</th>
			</tr>
		</thead>
		<tbody id="users" class="list:user user-list">
			<?php
				$count = 0;
				foreach($resuth1 as $auser)
				{
					$auser = apply_filters("pmpro_members_list_user", $auser);
					//get meta
					$theuser = get_userdata($auser->ID);
                                        $customer_orders = get_posts( array(
                                             'numberposts' => -1,
                                             'meta_key'    => '_customer_user',
                                             'meta_value'  => $auser->ID,
                                             'post_type'   => wc_get_order_types(),
                                             'post_status' => array_keys( wc_get_order_statuses() ),
                                        ));

                                        $loop = new WP_Query($customer_orders);
                                        $invoice_ids = $order_ids = array();

                                        foreach ($customer_orders as $orderItem){
                                        $order = wc_get_order($orderItem->ID);
                                        $arr = array_values(array_filter($order->meta_data, function($ar) {
                                          return ($ar->key == '_wcpdf_invoice_number');
                                        }));   
                                       
                                        
                                        $invoice_ids[] = $arr[0]->value;
                                        $order_ids[] = $order->ID;
                                        }
					?>
						<tr <?php if($count++ % 2 == 0) { ?>class="alternate"<?php } ?>>
							<td><?php echo $theuser->ID?></td>
							<td class="username column-username">
								<?php echo get_avatar($theuser->ID, 32)?>
								<strong>
									<?php
										$userlink = '<a href="user-edit.php?user_id=' . $theuser->ID . '">' . $theuser->user_login . '</a>';
										$userlink = apply_filters("pmpro_members_list_user_link", $userlink, $theuser);
										echo $userlink;
									?>
								</strong>
								<br />
								<?php
									// Set up the hover actions for this user
									$actions = apply_filters( 'pmpro_memberslist_user_row_actions', array(), $theuser );
									$action_count = count( $actions );
									$i = 0;
									if($action_count)
									{
										$out = '<div class="row-actions">';
										foreach ( $actions as $action => $link ) {
											++$i;
											( $i == $action_count ) ? $sep = '' : $sep = ' | ';
											$out .= "<span class='$action'>$link$sep</span>";
										}
										$out .= '</div>';
										echo $out;
									}
								?>
							</td>
							<td><?php echo $theuser->first_name?></td>
							<td><?php echo $theuser->last_name?></td>
							<td><a href="mailto:<?php echo esc_attr($theuser->user_email)?>"><?php echo $theuser->user_email?></a></td>
<td><?php echo implode(',', $invoice_ids); ?></td>
<td><?php echo implode(',', $order_ids); ?></td>
							<?php do_action("pmpro_memberslist_extra_cols_body", $theuser);?>
							<td>
								<?php
									echo pmpro_formatAddress(trim($theuser->pmpro_bfirstname . " " . $theuser->pmpro_blastname), $theuser->pmpro_baddress1, $theuser->pmpro_baddress2, $theuser->pmpro_bcity, $theuser->pmpro_bstate, $theuser->pmpro_bzipcode, $theuser->pmpro_bcountry, $theuser->pmpro_bphone);
								?>
							</td>
							<td><?php echo $auser->membership?></td>
							<td>
								<?php if((float)$auser->initial_payment > 0) { ?>
									<?php echo pmpro_formatPrice($auser->initial_payment);?>
								<?php } ?>
								<?php if((float)$auser->initial_payment > 0 && (float)$auser->billing_amount > 0) { ?>+<br /><?php } ?>
								<?php if((float)$auser->billing_amount > 0) { ?>
									<?php echo pmpro_formatPrice($auser->billing_amount);?>/<?php if($auser->cycle_number > 1) { echo $auser->cycle_number . " " . $auser->cycle_period . "s"; } else { echo $auser->cycle_period; } ?>
								<?php } ?>
								<?php if((float)$auser->initial_payment <= 0 && (float)$auser->billing_amount <= 0) { ?>
									-
								<?php } ?>
							</td>
							<td><?php echo date_i18n(get_option("date_format"), strtotime($theuser->user_registered, current_time("timestamp")))?></td>
							<td>
								<?php
									if($auser->enddate)
										echo apply_filters("pmpro_memberslist_expires_column", date_i18n(get_option('date_format'), $auser->enddate), $auser);
									else
										echo __(apply_filters("pmpro_memberslist_expires_column", "Never", $auser), "pmpro");
								?>
							</td>
						</tr>
					<?php
				}

				if(!$theusers)
				{
				?>
				<tr>
					<td colspan="9"><p><?php _e("No members found.", "pmpro");?> <?php if($l) { ?><a href="?page=pmpro-memberslist&s=<?php echo esc_attr($s);?>"><?php _e("Search all levels", "pmpro");?></a>.<?php } ?></p></td>
				</tr>
				<?php
				}
			?>
		</tbody>
	</table>
        
 <?php   
}


function my_custom_menu_page(){
		global $wpdb;
		//$resuth=$wpdb->get_results( "SELECT distinct(meta_key) FROM $wpdb->usermeta" );
		//$resuth=$wpdb->get_results($sql1);

    
		echo "<a target='_blank' class='button export' href='/export.php' style='margin-top:30px'>Export CSV</a>";
		echo "<div class='table-container'><table id='example' class='table table-striped table-bordered' cellspacing='0 width='100%'>
		<thead>
		<tr><th>Id</th>
                <th>Name</th>
                <th>Email</th>
				<th>SKIN TONE</th>
				<th>SKIN TYPE</th>
				<th>HAIR TYPE</th>
				<th> Nail </th>
				<th> LIP COLORS</th>
				<th> EYE MAKEUP</th>
                <th>COMF WITH MAKEUP</th>
                <th>JEWELLERY</th>
                <th>PRODUCTS </th>
                </tr></thead><tbody>";

	     
$args1 = array(
 'role' => 'customer',
 'orderby' => 'user_nicename',
 'order' => 'ASC'
);
 $subscribers = get_users($args1);

 foreach ($subscribers as $user) {
   $userid= $user->ID;
  $skintone = get_user_meta( $userid, 'skin_tone', true );
  if($skintone !=''){
     echo '<tr>';
 echo '<td>' . $user->ID. '</td>';
 echo '<td>' . $user->display_name. '</td>';
  echo '<td>' . $user->user_email . '</td>';
 echo '<td>' . get_user_meta($user->ID,'skin_tone',true) . '</td>';
 echo '<td>' . get_user_meta($user->ID,'skin_type',true) . '</td>';
 echo '<td>' . get_user_meta($user->ID,'hair',true) . '</td>';
 echo '<td>' . get_user_meta($user->ID,'comfrt_nail',true) . '</td>';
 echo '<td>' . get_user_meta($user->ID,'comfrt_lip',true) . '</td>';
 echo '<td>' . get_user_meta($user->ID,'comfrt_eye',true) . '</td>';
 echo '<td>' . get_user_meta($user->ID,'comfrt_makeup',true) . '</td>';
 echo '<td>' . str_replace(',',' | ',get_user_meta($user->ID,'brand',true)) . '</td>';
 echo '<td>' . str_replace(',',' | ',get_user_meta($user->ID,'makeup',true)) . '</td>';
echo '</tr>';
}
 }


             echo "</tbody></table></div>";	

?>


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>

<script>
jQuery(document).ready(function() {
    jQuery('#example').DataTable();
} );
</script>
<style>
#example_wrapper {
  float: left;
  margin-top: 20px;
  width: 98.5%;
}
table#example {
  border: medium none;
  padding-top: 20px;
}
table.dataTable thead .sorting_asc {
  background:#333333;
}
table.dataTable thead .sorting {
  background:#333333;
}
table#example thead tr th {
  color: #fff;
  text-align: left;
}
table#example tr td {
  background: none repeat scroll 0 0 #FFFFFF;
  border-bottom: 1px solid #DCDCDC;
  border-right: 1px solid #DCDCDC;
  padding: 8px 18px;
}
#example_paginate {
  margin-top: 10px;
}
#example_paginate .paginate_button {
  background: none repeat scroll 0 0 #333333;
  color: #FFFFFF !important;
}

.ftxt {
  float: left;
  width: 100%;
}
form#postcode label {
  float: left;
  padding-bottom: 5px;
  width: 100%;
}
form#postcode .input[type="text"] {
  border: medium none;
  box-shadow: none;
  float: left;
  height: 30px;
  width: 31.5%;
}
form#postcode .input[type="text"] {
  border: none;
  box-shadow: none;
  float: left;
  height: 30px;
  width: 31.5%;
bac
}
form#postcode .fbtn input[type="submit"]{
  background:#333;
  box-shadow: none;;
  color: #FFFFFF;
  font-size: 16px;
  height: 40px;
  margin-top: 10px;
  width: 130px;
}

form#postcode .fbtn input[type="submit"]:hover {
  background: none repeat scroll 0 0 #333333;
  color: #FFFFFF;
}
</style>


<?php

}
?>
