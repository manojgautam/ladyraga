<?php
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename="'.lang('clients').'.csv"');

$output = fopen('php://output', 'w');
$html = array(lang('persname'), lang('eCategory'),lang('eMobile'),lang('eEmail'),lang('eDOB'), lang('eADD'), lang('eCITY'),lang('eSTATE'),lang('ePIN'), lang('eASSOCIATION'), lang('eNATURE'),lang('eBUSINESS'), lang('eINDUSTRY'),lang('ebADD'),lang('ebCITY'), lang('ebSTATE'),lang('ebPIN'),lang('ebLANDLINE'),lang('ebMOB'),lang('eWEBSITE'),lang('ebCIN'),lang('ebOTHERS'));

 
fputcsv($output, $html);
$i=1;
foreach ($clients as $new){
        if($new->dob=='0000-00-00'){$thedate='';}
        else{$thedate=date_format(date_create($new->dob),'d-m-Y');}
	$html1 = array($new->name,$new->case_category_id,$new->contact,$new->email,$thedate,$new->address1,$new->city,$new->state,$new->pin, $new->bassociation,$new->business_organization_type,$new->bname,$new->business_industry_type,$new->baddress,$new->bcity,$new->bstate,$new->bpin,$new->blandline,$new->bcontact, $new->bwebsite,$new->cin,$new->others);
	fputcsv($output, $html1);
	 $i++;
}

fclose($output);
?>
