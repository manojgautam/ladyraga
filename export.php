<?php
require_once( 'wp-load.php' );
global $wpdb;
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename="list.csv"');

$output = fopen('php://output', 'w');
$html = array('Name', 'Email', 'SKIN TONE', 'SKIN TYPE', 'HAIR TYPE', 'Nail', 'LIP COLORS', 'EYE MAKEUP', 'COMF WITH MAKEUP', 'JEWELLERY', 'PRODUCTS');

 
fputcsv($output, $html);
$args1 = array(
 'role' => 'customer',
 'orderby' => 'user_nicename',
 'order' => 'ASC'
);
 $subscribers = get_users($args1);
 
foreach ($subscribers as $user){
	$userid= $user->ID;
  $skintone = get_user_meta( $userid, 'skin_tone', true );
  if($skintone !=''){
 $html1 = array($user->display_name,$user->user_email,get_user_meta($user->ID,'skin_tone',true),get_user_meta($user->ID,'skin_type',true),get_user_meta($user->ID,'hair',true),get_user_meta($user->ID,'comfrt_nail',true),get_user_meta($user->ID,'comfrt_lip',true),get_user_meta($user->ID,'comfrt_eye',true),get_user_meta($user->ID,'comfrt_makeup',true),get_user_meta($user->ID,'brand',true),get_user_meta($user->ID,'makeup',true));
 fputcsv($output, $html1);
  }
}

fclose($output);
?>